import numpy as np
from pyarrow.csv import read_csv, write_csv
import time
import sys

target_file = 'data/tripdata_small_2021_5.csv'
output_file = 'data/output_small_arrow.csv'
top_percentile = 0.9
read_times = {}
process_times = {}

# Do a small argument parsing, again, very crude, not even check if the file exists
if(len(sys.argv) not in [1,3]):
    print(f"Pass an input file and an output file, or none (defaults: python {sys.argv[0]} {target_file} {output_file} )")
    sys.exit(1)
# If there are input values, use them, if not, keep the hard coded defaults
if(len(sys.argv)>1):
    target_file = sys.argv[1]
    output_file = sys.argv[2]

# Better approach than np+pd; use a specialized library, designed for the task
# (py)arrow+numpy

### Read the data...
start = time.time()

df_pyarrow = read_csv(target_file)

read_time = time.time() - start

### Process the data...
start = time.time()

# Compute how many items are in the top 10% (you guessed it, total*0.1)
top_k = int(df_pyarrow.num_rows*(1-top_percentile))
# Get the sorted indices in ascending order by trip distance, and keep the top k
top_k = np.argsort(df_pyarrow['trip_distance'][-top_k:])
# Create a mask (maybe there is a numpy function that does this more elegantly, this way just does it)
mask_top_k = np.zeros(df_pyarrow.num_rows, dtype='bool')
mask_top_k[top_k] = True
# Filter the rows with the mask using pyarrow
# A (Tiny) detail here; the task does not mention ordered output, so we can just filter the records
out = df_pyarrow.filter(mask_top_k)

process_time = time.time() - start

### Save the output
start = time.time()
write_csv(out, 'out_arrow.csv')
write_time = time.time() - start

print(f"Arrow read time: {read_time*1000:.3f} ms")
print(f"Arrow process time: {process_time*1000:.3f} ms")
print(f"Arrow write time: {write_time*1000:.3f} ms")
print(f"\nArrow total time: {(read_time+process_time+write_time)*1000:.3f} ms")