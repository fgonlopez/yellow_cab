#!/bin/bash
if ! [ -f tripdata_small_2021_5.csv ]; then
  curl https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2021-05.csv -o data/tripdata_small_2021_5.csv
fi

if ! [ -f tripdata_big_2013_5.csv ]; then
  curl https://s3.amazonaws.com/nyc-tlc/trip+data/yellow_tripdata_2013-05.csv -o data/tripdata_big_2013_5.csv
fi
