import numpy as np
import pandas as pd
import time
import sys

target_file = 'data/tripdata_small_2021_5.csv'
top_percentile = 0.9
read_times = {}
process_times = {}

# Do a small argument parsing, again, very crude, not even check if the file exists
if(len(sys.argv) not in [1,3]):
    print(f"Pass an input file and an output file, or none (defaults: python {sys.argv[0]} {target_file} {output_file} )")
    sys.exit(1)
# If there are input values, use them, if not, keep the hard coded defaults
if(len(sys.argv)>1):
    target_file = sys.argv[1]
    output_file = sys.argv[2]

# Let's do a time comparison between approaches
# Straightforward approach, we don't care much about losing a few seconds (warning: it's bad)
# Use numpy+pandas for everything

### Read the data...
start = time.time()

# low_memory=False makes pandas not read in batches, otherwise it throws a warning on type inference
# we could avoid it by setting the types of all columns beforehand, but given the scope of this task
# I don't think it's a good idea
df_pandas = pd.read_csv(target_file, low_memory=False)

read_time = time.time() - start


### Process the data...
start = time.time()

# We could combine more steps in a single line,
# but I usually prefer keeping lines "specialized" for readability
top_k = int(len(df_pandas)*(1-top_percentile))
top_k = np.argsort(df_pandas['trip_distance'])[-top_k:]
out = df_pandas.iloc[top_k]
process_time = time.time() - start

### Save the output
start = time.time()
out.to_csv('out_pandas.csv')
write_time = time.time() - start

print(f"Pandas read time: {read_time*1000:.3f} ms")
print(f"Pandas process time: {process_time*1000:.3f} ms")
print(f"Pandas write time: {write_time*1000:.3f} ms")
print(f"\nPandas total time: {(read_time+process_time+write_time)*1000:.3f} ms")