# Task
> **What should I do?**
> Using NYC "Yellow Taxi" Trips Data, return all the trips over 0.9 percentile in distance traveled for any of the CSV files you can find there.

---
# Results
> **What should I send?**
> A repo with all the code you used and a readme (in english) explaining your approach, the steps to reproduce your test and everything you feel is important.

---
### Code
**1. download_data.sh**
	Simple convenience script that downloads two files from the NYC Yellow Taxi Trips Data, a smaller one (\~0.2GB), meant for quick testing, and a bigger one (\~2.4GB), meant to stress the procedures. Downloads the files to in ./data/
	
**2. process_shell.sh**
	A Linux shell script, using command line tools to process the data. It should receive two arguments; first, the name of the input file (one of the .csv files with the Trips data) and the name of the desired output file.
	
**3. process_pandas.py**
	A Python script to process the data using pandas. It should receive two arguments; first, the name of the input file (one of the .csv files with the Trips data) and the name of the desired output file.
	
**4. process_arrow.py**
	A Python script to process the data using pyarrow. It should receive two arguments; first, the name of the input file (one of the .csv files with the Trips data) and the name of the desired output file.
	

All process_\* scripts are functionally similar; they take two parameters and output a file with the required output for the task. Both Python scripts also report the time in milliseconds (measured with Python\'s time.time()) it takes to:

* Read the csv input file
* Process the data (i.e. filter the records with \'trip_distance\' over 0.9 percentile)
* Write the output to a csv file
* Sum of all of the above

---
### Steps to reproduce

1. Ensure that you have the required terminal tools installed. The tools are *curl* for the download script, and *cat*, *wc*, *sort* and *head* for the processing script. These are tools commonly available by default on all Linux distributions. If some of these is not present in your system, use your preferred package manager to get them.
2. Ensure that you have the required python and libraries versions. These scripts should work with the latest Python and libraries, but for the sake of reproducibility:

* Python 3.8.8

* Numpy 1.21.4

* Pandas 1.3.4

* Pyarrow 6.0.0

From now on, I will assume your **current working directory** is the repo folder.

It is also recommended to create a python virtual environment for testing. You can use your favourite virtual environment manager, but I suggest using venv:

> python -m venv env
> source env/bin/activate

When you are done testing the procedures, you can deactivate the enviroment with:
> deactivate

Install the libraries using pip (since they are just 3, and this is a technical test, I will not provide a requirements file specifying the versions, which would be the standard procedure):
> pip install numpy pandas pyarrow

3. Now that the everything is set up, run the data download script (or download whichever files you would like manually) and the processing scripts:
	1. The download script:
		> \# If there is no data folder in the directory, create it with
		> mkdir data
		> sh download_data.sh
		
		After the download script, we should have populated the directory _./data/_ with two files:

		* tripdata_small_2021_5.csv - Smaller csv file, about 200MB in size.

		* tripdata_big_2013_5.csv - Bigger csv file, about 2.4GB in size.
		
\
	From now on, these can be run in any order.
	
	2. Each processing script (using Linux\'s _time_):

	* Python (Pandas version): 

	> time python process_pandas.py \<input_file.csv\> \<output_file.csv\>

	* CLI tools version:

	> time sh process_shell.sh \<input_file.csv\> \<output_file.csv\>

	* Python (Pyarrow version):

	> time python process_arrow.py \<input_file.csv\> \<output_file.csv\>

---
### The three approaches, explained

\* There is a time comparison between these at the end.
All of the files have comments, but to outline the most important parts:

**1. Pandas:**
	This approach is meant to represent what a typical Python user would do when presented with the task; use Pandas. This approach has two important characteristics:
	1. Reading and writing .csv files suing Pandas is *slow*, since the reader/writer are not multithreaded. This can be circumvented relatively easily using another library to do it (like Pyarrow!).
	2. To find the values above 0.9 percentile of \'trip_distance\', we may be tempted to use df.sort_values(\'trip_distance\') and keep the top 0.1 records; this would tank performance, because we would be working to rearrange the full dataset, only to drop 90% of it afterwards! Instead, we can use np.argsort() to get the sorted indices, keep the top 0.1 indices, and use df.iloc[indices] to extract the desired records from the dataframe.
	
**2. Pyarrow:**
	Pyarrow is a Python wrapper for Arrow (which is mostly C++). For starters, after a little testing, the csv read/write operations are pretty fast... Why not try to leverage this specialized library as much as possible?
	1. Arrow handles the data in chunks (even in memory!), which allows it to read and write data lightning fast (if your processor can spare a few cores and cycles). As you can see in the screenshots at the end, the read/write times are an order of magnitude lower than Pandas\'.
	2. Since the data is chunked, we want to avoid rearranging the data, as it would mean breaking the current arrangement to make a new one which means operations. Which means time, and time is *precious!*. Lucky for us, Pyarrow data containers have a very handy method: *df.filter(mask)*. Again, this operation can be applied in parallel. We just need to create a mask (I used Numpy\'s argsort and a boolean array), and apply it to the data using filter. Note that this does not *sort* the data itself, just filters the records that are over 0.9 percentile, keeping the same relative order as in the original data (since ordering the output is not mentioned in the task, I decided not to sort the data).
	
**3. CLI Tools:**
	Why not use some time-tested Linux tools? The requested operation is essentially a row count, sorting and keeping the top 0.1 rows. Lucky for us, there are three Linux tools that do exactly that; *wc*, *sort* and *head*.
	1. These tools are *fast*, the pipe operator works like a charm to connect them, and I would be hard pressed to find a Linux computer *without* these tools already installed.
	2. The only thing we need to know about the data is how many rows we will want to keep, which is 10% of the total (since we want those above 0.9 percentile in \'trip_distance\'). To get that number, we can use wc -l to get the number of rows in the file, substract 1 (since we do not care about the header), and multiply by 0.1. Now, we need to keep that number of rows from the *sorted* data, which can be obtained by using sort (with a few arguments to make it super fast), and keep the top 0.1 rows with head -n \<number of rows to keep\>
	

Finally, if I had to choose an approach, it would be between the CLI tools or pyarrow, and it would really depend on the end user. If they do not have the required expertise in Python, the script, as simple as it may be, might look like a black box (faster, but still a black box), while the CLI Linux tools are simpler to understand, and much more commonly known across all IT profiles, and they are well documented. On the other hand, if the speed requirement is more important, pyarrow is the clear winner. As a side note, if this was not a technical test, I would have tried a DBMS approach (load the data to Postgres, for instance, and implement the procedure in SQL), or a third party data warehousing/processing service as well.


Sample execution for the smaller file (by default, the scripts use the \"small\" dataset):
![sample_small](https://bitbucket.org/fgonlopez/yellow_cab/raw/ade973ef53bd65f7e13a1af0e9d6824d57cc58e2/comparison_small.png)

Sample execution for the bigger file:
![sample_small](https://bitbucket.org/fgonlopez/yellow_cab/raw/ade973ef53bd65f7e13a1af0e9d6824d57cc58e2/comparison_big.png)



### Additional points (based on [these](https://javisantana.com/fastdata/data-engineer-skills.html))
**1. Reproducibility** - All of these scripts are meant to use any of the .csv files in the dataset interchangeably. Even more, any .csv file that has a \"trip_distance\" field will work with the Python scripts! The shell script does not even need the column to be called like that, it takes the 6th column regardless.

**2. Testability** - All of these scripts produce a .csv file with a format similar to the input .csv, which means that output inmutability after a change in the code can be tested simply by using diff current_output.csv expected_output.csv

**3. Performance** - While Pandas\' speed is not fit for any serious repeated process (I assume this is not an operation that we will compute just a few times), both the shell script and pyarrow offer similar performance for the small file, but pyarrow is about ~1.75 times faster for the bigger file. But let us not forget about the another aspect of performance other than time; space (often overlooked, rightly so most times, because memory is usually cheaper), there is no free lunch. All of these algorithms are memory intensive, as they all load a big .csv file into memory in order to make the sorting operation as fast as possible. But depending on the conditions (imagine we are limited to an embedded system that can not fit the whole data in memory at once) we might want to use other options. The good part is that these methods do not consume much overhead space, as they do not rely on having a system just to store and handle the data (i.e. a DBMS).

**4. Design decisions** - I have briefly touched on the previous point; the design decisions have favoured lowering execution time, because given no other resource constraint (e.g. memory, lines of code or libraries), it is what makes the most sense. Hence why I have not used any intermediate storages, all the scripts take the initial input and output the final result. Another design feature, that was not really a decision, but could be interesting to explore, is to use a parametric statistics approach to outlier detection. If the data fits a known distribution (for instance, a Gaussian distribution) computing outliers from that distribution is much faster, as we only need to know the characteristics of the distribution (mean and standard deviation for a Gaussian) that the data follows (of course we would have to implement some way to continuously make sure that the data still follow the same distribution, like "if more than _x%_ of the data in a _y_ time period is marked as outlier, raise an alarm"). For instance, the COVID situation would have probably affected these types of models.

**5. Data quality** - I have implemented essentially no explicit data quality checks, so the only checks that are in place are those performed by the libraries. That is not to say that I have not thought about them, or know their behaviour; since I do not know what to do with missing values, they are just interpreted as what they are, \"unknown\", and thus kept in the resulting data. This means that a significant (\*in respect to that top 10% data that is kept) data quality problem is easy to detect, as the resulting output will contain all the null/problematic values (due to the . I assume the idea is that the procedure should work as (part of) an outlier detection, so I think the best option is to keep them.

**6. Others** - I believe, given the simplicity and the use of common libraries and tools, that the first sections of this readme should be enough documentation for a user of these scripts.
Also, due to how easi it is to reproduce and test the procedures, along with their simplicity, CI should be straightforward (I started off coding the procedures in isolated interchangeable modules, but stopped after a few minutes, as it made the code harder to understand at a glance, which I believe to be more important during a technical test).
As for **evolution** (and to tie in with the next points), I would say:

* **First**, if a more memory intensive procedure was needed (say, we could want to compute the 0.9 percentile out of all the .csv files, and they do not fit in memory all at once), I would use a DBMS (like Postgres), or a cloud service, depending on the situation, and implement the procedures using SQL directly (or whichever language the DBMS supports). Since these are historical records, we can assume that they do not change over time (i.e. what happens in June does not change any rows from February), so the data lends itself pretty well to being stored in a DBMS over the current method (many .csv files).

* **Second**, how to make this _**real time**_. An option would be to keep a \"top ranking\" (i.e. a priority queue) table that expands dynamically in size to be 10% of the amount of data in the system. The main problem is that it might potentially require to keep the data stored in order (when the table \"expands\" you have to potentially look through all the data for the values that need to fill the expanded space), which might make it hard to scale.

* **Third, another (more scalable) approach** to make this *real time* (which I believe to be a redundancy, after I have had to deal with a very similar problem in my current position, computing percentiles). Assuming that we are talking about a streaming system that receives the information in the rows as events, rather than just a csv file with all the records, we could split the algorithm in two parts; a processing part, and a \"reference value update\" part. The idea is that the processing update part just takes all the currently stored records, computes the desired percentile (in this case, it would just be a single number; the value of the 90th percentile, but you would usually want to compute a table with many entries to estimate more percentiles that you might want to use. Let's stick with the single number, as it is what is described in the task). Given the reference value that corresponds to the 90th percentile, the \"main\" outlier detection process just needs to retrieve the reference value from a common storage, and check wether the inputs are over the reference value (the 90th percentile). This also requires a strategy for updating the reference value as it is needed (which can be done every x amount of time, or every x insertions). The problem with this approach is that you are not computing *exactly* if the input is over the 0.9 percentile, as an insertion changes every single percentile of , but depending on the conditions, it should be pretty close (given enough values in the set, \"moving\" a mean can be done with very extreme values, but \"moving\" a percentile is far slower).
	
**7. How does this help answer questions** - The procedure answers mainly to the initial question; what are the records that are over 0.9 percentile in trip_distance? The usefulness of the answer, I believe, is twofold; first, it allows the entities that generate the values in the file to check for problems in their systems (e.g. the hardware that computes and reports the data is malfunctioning, process along the chain that moves the data from the input to the csv file is failing), and second, it allows other systems downstream to analyse or further process these data marked as outliers (e.g. look for different types of outliers clustering them). This is, more or less, the task of the data engineer; laying the paths between data producers and consumers, delivering value to all of them.









