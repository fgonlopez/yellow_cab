#!/bin/bash

# This script needs 2 arguments; the input file, and the output file

# I'm going to include a crude arg parsing:
# Assert that there are 2 arguments
if ! [ $# -eq 2 ]; then
  echo "Pass a single file to process and an output file"
  exit 1
fi
# Assert that the input file exists
if ! [ -f $1 ]; then
  echo "File" $1 "does not exist"
  exit 1
fi

# Count the number of rows in the input file
ROWS=$(cat $1 | wc -l)
# Read the file, aand sort it using:
# LANG=C -> Change the locale to the simplest, "C"
# --parallel=16 -> Use 16 threads
# -S 50% -> Set buffer size to 50% of memory
# -k5 -> Sorting key
# -n -> Compare using numerical values
# -r -> Reverse (default is ascending, changes to descending)
# -t "," -> Set the separator to comma
# head -n $((ROWS / 10)) -> Keeps the topmost $(((ROWS-1)/10)) rows (top 10% accounting for the header line, though it probably won't change much)
cat $1 | LANG=C sort --parallel=16 -S 50% -k5 -n -r -t "," | head -n $(((ROWS-1) / 10)) > $2
